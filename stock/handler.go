package stock

import (
	"encoding/json"
	"go-api-ws/helpers"
	"net/http"
)

var (
	stockSlice []DataStock
)

func init() {
	GetConfig("config.yaml")
}

func checkStock(w http.ResponseWriter, r *http.Request) {
	itemSkuFromUrl, err := helpers.GetParameterFromUrl("sku", r)
	helpers.PanicErr(err)
	var item DataStock
	item.GetDataFromDbBySku(itemSkuFromUrl)
	response := helpers.Response{
		Code:   http.StatusOK,
		Result: item}
	response.SendResponse(w)
}

func insertToStock(w http.ResponseWriter, r *http.Request) {
	var stockData DataStock
	err := json.NewDecoder(r.Body).Decode(&stockData)
	helpers.PanicErr(err)
	defer helpers.CloseAndCheckError(r.Body)
	stockData.insertDataToStock()
	helpers.WriteResultWithStatusCode(w, "ok", http.StatusOK)
}

func updateStockItem(w http.ResponseWriter, r *http.Request) {
	var stockData DataStock
	err := json.NewDecoder(r.Body).Decode(&stockData)
	helpers.PanicErr(err)
	defer helpers.CloseAndCheckError(r.Body)
	stockData.updateDataInDb()
	helpers.WriteResultWithStatusCode(w, "ok", http.StatusOK)
}

func removeItemFromStock(w http.ResponseWriter, r *http.Request) {
	itemSkuFromUrl, err := helpers.GetParameterFromUrl("sku", r)
	helpers.PanicErr(err)
	removeItemFromDb(itemSkuFromUrl)
	helpers.WriteResultWithStatusCode(w, "ok", http.StatusOK)
}

func bulkInsert(w http.ResponseWriter, r *http.Request) {
	err := json.NewDecoder(r.Body).Decode(&stockSlice)
	helpers.PanicErr(err)
	defer helpers.CloseAndCheckError(r.Body)
	if ConfigStock.Strategy == "1" {
		for index := range stockSlice {
			stockSlice[index].insertDataToStock()
		}
	}
	helpers.WriteResultWithStatusCode(w, "ok", http.StatusOK)

}
