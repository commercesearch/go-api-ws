package paypal

import (
	"encoding/json"
	"github.com/netlify/PayPal-Go-SDK"
	"go-api-ws/helpers"
	"net/http"
	"os"
	"fmt"
)

const (
	user        = "AQAxERGzVjPLWcjJKoLcc-dGZEApF4dnDs_JO1vAJhhSzze_eCcq-CuewzyOHZHpq7UafyKQi9A6LLwM"
	pass        = "ENxGS5wZJ-tneOYgnB3kGuJbQANu_FfCC0Fgyaf9SlvqLCfe70FVlbchr0ejmkrSUVfx1GWRG7b5O2H0"
	paypalApi   = "https://api.sandbox.paypal.com"
	contentType = "application/json"
	redirectURL = "http://www.duckduckgo.com"
	cancelURL   = "https://www.google.lt"
)

func Create(w http.ResponseWriter, r *http.Request) {

	//Creating a request and decode request body into it
	var requestFromClient request
	err := json.NewDecoder(r.Body).Decode(&requestFromClient)
	helpers.PanicErr(err)

	//Getting the client from the api base SandBox(for testing)
	client, err := paypalsdk.NewClient(user, pass, paypalsdk.APIBaseSandBox)
	helpers.PanicErr(err)
	err = client.SetLog(os.Stdout)
	helpers.CheckErr(err)

	// GetAccessToken returns struct of TokenResponse
	_, err = client.GetAccessToken()
	helpers.PanicErr(err)

	//We get the Amount that needs to be paid (currency, total and details)
	amount := paypalsdk.Amount{
		Total:    requestFromClient.Transactions[0].Amount.Total,
		Currency: requestFromClient.Transactions[0].Amount.Currency}

	// CreateDirectPaypalPayment sends request to create a payment with payment_method=paypal
	paymentResult, err := client.CreateDirectPaypalPayment(amount, redirectURL, cancelURL, "")
	helpers.PanicErr(err)
	helpers.WriteJsonResult(w, paymentResult)
}

func Execute(w http.ResponseWriter, r *http.Request) {
	var requestFromClient request
	err := json.NewDecoder(r.Body).Decode(&requestFromClient)
	helpers.PanicErr(err)

	client, err := paypalsdk.NewClient(user, pass, paypalsdk.APIBaseSandBox)
	helpers.PanicErr(err)
	err = client.SetLog(os.Stdout)
	helpers.CheckErr(err)
	_, err = client.GetAccessToken()
	helpers.PanicErr(err)

	executeResult, err := client.ExecuteApprovedPayment(requestFromClient.PaymentID, requestFromClient.PayerID)
	helpers.PanicErr(err)

	fmt.Println("@@@@@@@")
	fmt.Println(w)
	fmt.Println("@@@@@@@")
	fmt.Println(executeResult)
	fmt.Println("@@@@@@")

	helpers.WriteJsonResult(w, executeResult)

}
