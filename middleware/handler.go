package middleware

import (
	"go-api-ws/auth"
	"go-api-ws/helpers"
	"log"
	"net/http"
)

const (
	role = "user"
)

func ProtectedEndpoint(handlerFunc http.HandlerFunc) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		urlToken, err := helpers.GetTokenFromUrl(request)
		helpers.PanicErr(err)
		token := auth.ParseToken(urlToken)
		claims, err := auth.GetTokenClaims(token)
		helpers.CheckErr(err)
		if err != nil {
			helpers.WriteResultWithStatusCode(writer, "Invalid token", http.StatusBadRequest)
		} else {
			if claims["role"] == role && auth.CheckIfTokenIsNotExpired(claims) {
				handlerFunc.ServeHTTP(writer, request)
			} else {
				helpers.WriteResultWithStatusCode(writer, "Token expired", http.StatusForbidden)
			}
		}
	}
}

func Log(handlerFunc http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		log.Println("Before")
		defer log.Println("After")
		handlerFunc.ServeHTTP(writer, request)
	})
}